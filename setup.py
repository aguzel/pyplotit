#!/usr/bin/env python
# Copyright (c) 2021, Université catholique de Louvain
#
# Distributed under the GNU General Public License, see accompanying file LICENSE
# or https://gitlab.cern.ch/cp3-cms/pyplotit for details.

from setuptools import setup

setup()
