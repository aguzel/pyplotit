"""
Pythonic access to TH1, TH2 and TGraph data
"""
__all__ = ("bins", "points")

from functools import partial
from itertools import product

from cppyy import gbl
from future.utils import iteritems


def bins(hist):
    """
    Get access to the bins of a histogram

    the returned object can be indexed and iterated over,
    the elements are of the appropriate dimensionality
    """
    return hist.__bins__()


def points(graph):
    """
    Get access to the points of a graph

    the returned object can be indexed and iterated over,
    the elements are of the appropriate dimensionality
    """
    return graph.__points__()


axisBinDescriptors = {
    "center": gbl.TAxis.GetBinCenter,
    "lowEdge": gbl.TAxis.GetBinLowEdge,
    "upEdge": gbl.TAxis.GetBinUpEdge,
    "width": gbl.TAxis.GetBinWidth,
    "label": gbl.TAxis.GetBinLabel,
}
histBinDescriptors = {
    "content": gbl.TH1.GetBinContent,
    "error": gbl.TH1.GetBinError,
    "lowError": gbl.TH1.GetBinErrorLow,
    "upError": gbl.TH1.GetBinErrorUp,
}


# trivial helper: add doc and return (for lambdas)
def _addDoc(obj, doc=None):
    obj.__doc__ = doc
    return obj


graphPointDescriptorsPerAxis = {
    "{0}": lambda ax: _addDoc(
        (lambda self, i: getattr(gbl.TGraph, f"Get{ax.upper()}")(self)[i]),
        doc=f"lambda g,i : gbl.TGraph.Get{ax.upper()}(g)[i]",
    ),
    "{0}Error": lambda ax: getattr(gbl.TGraph, f"GetError{ax.upper()}"),
    "{0}HighError": lambda ax: getattr(gbl.TGraph, f"GetError{ax.upper()}high"),
    "{0}LowError": lambda ax: getattr(gbl.TGraph, f"GetError{ax.upper()}low"),
}

################################################################################
# Property helper classes                                                      #
################################################################################


class BinProperty1(property):
    """
    Easily construct many properties of the type

    >>> @property
    >>> def fun(self):
    >>>     return self._h.getter(self._i)
    """

    def __init__(self, accessor, idxAttr):
        self.accessor = accessor
        self.idxAttr = idxAttr
        property.__init__(self, fget=None, fset=None, fdel=None)

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self
        return self.accessor(obj._h, getattr(obj, self.idxAttr))


class BinProperty2(property):
    """
    Easily construct many properties of the type

    >>> @property
    >>> def fun(self):
    >>>     return self._h.getter(self._i, self._j)
    """

    def __init__(self, accessor, xIdxAttr, yIdxAttr):
        self.accessor = accessor
        self.xIdxAttr = xIdxAttr
        self.yIdxAttr = yIdxAttr
        property.__init__(self, fget=None, fset=None, fdel=None)

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self
        return self.accessor(obj._h, getattr(obj, self.xIdxAttr), getattr(obj, self.yIdxAttr))


################################################################################
# Histogram axis                                                               #
################################################################################


class AxisBin1D:
    """
    iterator referring to a bin of an axis
    """

    __slots__ = ("_h", "_i")

    def __init__(self, axis, i):
        self._h = axis
        self._i = i

    def __repr__(self):
        return "AxisBins1D({})[{:n}]".format(repr(self._h), self._i)


# AxisBin1D data descriptors
for name, getter in iteritems(axisBinDescriptors):
    prop = BinProperty1(getter, "_i")
    prop.__doc__ = f"Bin {name} using ROOT.TAxis.{getter.__name__}"
    setattr(AxisBin1D, name, prop)


class AxisBins1D:
    """
    pythonic access to an axis' bins

    Not strictly a sequence since ROOT's bin numbering is preserved,
    but forward iteration and random access are supported.
    """

    __slots__ = ("_a",)

    def __init__(self, axis):
        self._a = axis

    def __repr__(self):
        return "AxisBins1D({})".format(repr(self._a))

    def __iter__(self):
        for i in range(1, self._a.GetNbins() + 1):
            yield AxisBin1D(self._a, i)

    def __len__(self):
        return self._a.GetNbins()

    def __getitem__(self, i):
        return AxisBin1D(self._a, i)


# decorate axis class
def _taxis_bins(self):
    return AxisBins1D(self)


gbl.TAxis.__bins__ = _taxis_bins

################################################################################
# One-dimensional histograms                                                   #
################################################################################


class HistoBin1D:
    """
    iterator referring to a bin of a one-dimensional histogram
    """

    __slots__ = ("_h", "_i")

    def __init__(self, hist, i):
        self._h = hist
        self._i = i

    def __repr__(self):
        return "{}({})[{:n}]".format(self.__class__.__name__, repr(self._h), self._i)


def _get1H(h, i, getter=None):
    return getter(h, i) / h.GetBinWidth(i)


def _xget(h, i, getter=None):
    return getter(h.GetXaxis(), i)


def _yget(h, i, getter=None):
    return getter(h.GetYaxis(), i)


# HistoBin1D data descriptors
for name, getter in iteritems(histBinDescriptors):
    prop = BinProperty1(getter, "_i")
    prop.__doc__ = f"Bin {name} using ROOT.TH1.{getter.__name__}"
    setattr(HistoBin1D, name, prop)
    # height instead of contents
    hGetterName = f"lambda h,i : ROOT.TH1.{getter.__name__}(h, i) / h.GetBinWidth(i)"
    hProp = BinProperty1(partial(_get1H, getter=getter), "_i")
    hProp.__doc__ = f"Bin {name} height using {hGetterName}"
    setattr(HistoBin1D, f"{name}H", hProp)
# Delegates to X-axis
for name, getter in iteritems(axisBinDescriptors):
    xGetterName = f"lambda h,i : ROOT.TAxis.{getter.__name__}(h.GetXaxis(), i)"
    prop = BinProperty1(partial(_xget, getter=getter), "_i")
    prop.__doc__ = f"Bin {name} using {xGetterName}"
    setattr(HistoBin1D, "x{}{}".format(name[:1].upper(), name[1:]), prop)


class HistoBins1D:
    """
    pythonic access to a (strictly) one-dimensional histogram's bins

    Not strictly a sequence since ROOT's bin numbering is preserved,
    but forward iteration and random access are supported.
    """

    __slots__ = ("_h",)

    def __init__(self, hist):
        self._h = hist

    def __repr__(self):
        return "HistoBins1D({})".format(repr(self._h))

    def __iter__(self):
        for i in range(1, self._h.GetNbinsX() + 1):
            yield HistoBin1D(self._h, i)

    def __len__(self):
        return self._h.GetNbinsX()

    def __getitem__(self, i):
        return HistoBin1D(self._h, i)


# decorate 1D histogram classes
def _th1_bins(self):
    return HistoBins1D(self)


gbl.TH1.__bins__ = _th1_bins
# placeholder for non-1D subclasses (2D is just below)
gbl.TH3.__bins__ = property(lambda: AttributeError("3D histograms are not supported yet"))

################################################################################
# Two-dimensional histograms                                                   #
################################################################################


class HistoBin2D:
    """
    iterator referring to a bin of a two-dimensional histogram
    """

    __slots__ = ("_h", "_i", "_j")

    def __init__(self, hist, i, j):
        self._h = hist
        self._i = i
        self._j = j

    def __repr__(self):
        return "{}({})[{:n},{:n}]".format(self.__class__.__name__, repr(self._h), self._i, self._j)


def _get2H(h, i, j, getter=None):
    return getter(h, i, j) / (h.GetXaxis().GetBinWidth(i) * h.GetYaxis().GetBinWidth(j))


# HistoBin2D data descriptors
for name, getter in iteritems(histBinDescriptors):
    prop = BinProperty2(getter, "_i", "_j")
    prop.__doc__ = f"Bin {name} using ROOT.TH2.{getter.__name__}"
    setattr(HistoBin2D, name, prop)
    # height instead of contents
    hGetterName = "lambda h,i,j : {}(h, i, j) / (h.GetXaxis().GetBinWidth(i)*h.GetYaxis().GetBinWidth(j)".format(
        getter.__name__
    )
    hProp = BinProperty2(partial(_get2H, getter=getter), "_i", "_j")
    hProp.__doc__ = f"Bin {name} height using {hGetterName}"
    setattr(HistoBin2D, f"{name}H", hProp)
# Delegates to axes
for name, getter in iteritems(axisBinDescriptors):
    # X axis
    xGetterName = f"lambda h,i : ROOT.TAxis.{getter.__name__}(h.GetXaxis(), i)"
    xProp = BinProperty1(partial(_xget, getter), "_i")
    xProp.__doc__ = f"Bin {name} using {xGetterName}"
    setattr(HistoBin2D, "x{}{}".format(name[:1].upper(), name[1:]), xProp)
    # Y axis
    yGetterName = f"lambda h,j : ROOT.TAxis.{getter.__name__}(h.GetYaxis(), j)"
    yProp = BinProperty1(partial(_yget, getter), "_j")
    yProp.__doc__ = f"Bin {name} using {yGetterName}"
    setattr(HistoBin2D, "y{}{}".format(name[:1].upper(), name[1:]), yProp)


class HistoBins2D:
    """
    pythonic access to a (strictly) two-dimensional histogram's bins

    Not strictly a sequence since ROOT's bin numbering is preserved,
    but forward iteration and random access are supported.
    """

    __slots__ = ("_h",)

    def __init__(self, hist):
        self._h = hist

    def __repr__(self):
        return "HistoBins2D({})".format(repr(self._h))

    def __iter__(self):
        for i, j in product(range(1, self._h.GetNbinsX() + 1), range(1, self._h.GetNbinsY() + 1)):
            yield HistoBin2D(self._h, i, j)

    def __len__(self):
        return self._h.GetNbinsX() * self._h.GetNbinsY()

    def __getitem__(self, idx):
        i, j = idx
        return HistoBin2D(self._h, i, j)


def _th2_bins(self):
    return HistoBins2D(self)


gbl.TH2.__bins__ = _th2_bins

################################################################################
# TGraph                                                                       #
################################################################################


class GraphPoint:
    """
    iterator referring to a point of a graph
    """

    __slots__ = ("_h", "_i")

    def __init__(self, graph, i):
        self._h = graph
        self._i = i

    def __repr__(self):
        return "GraphPoints({})[{:n}]".format(repr(self._h), self._i)


for ax in ("x", "y"):
    for namePat, getterGen in iteritems(graphPointDescriptorsPerAxis):
        name = namePat.format(ax)
        getter = getterGen(ax)
        prop = BinProperty1(getter, "_i")
        prop.__doc__ = "Point {n} using {func}".format(
            n=name, func=(getter.__name__ if getter.__name__ != "<lambda>" else getter.__doc__)
        )
        setattr(GraphPoint, name, prop)


class GraphPoints:
    """
    pythonic access to the points of a graph
    """

    __slots__ = ("_g",)

    def __init__(self, graph):
        self._g = graph

    def __repr__(self):
        return "GraphPoints({})".format(repr(self._g))

    def __iter__(self):
        for i in range(self._g.GetN()):
            yield GraphPoint(self._g, i)

    def __len__(self):
        return self._g.GetN()

    def __getitem__(self, i):
        return GraphPoint(self._g.GetN(), i)


# decorate 1D histogram classes
def _graph_points(self):
    return GraphPoints(self)


gbl.TGraph.__points__ = _graph_points
