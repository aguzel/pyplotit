---
jupytext:
  text_representation:
    extension: .mystnb
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.12.0
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Getting started

## Installation

[pyplotit](https://gitlab.cern.ch/cp3-cms/pyplotit) is a pure python package,
so the latest version can be installed with
```bash
pip install git+https://gitlab.cern.ch/cp3-cms/pyplotit.git
```
or, for an editable install when frequent updates and/or testing of changes
is expected, with
```bash
git clone https://gitlab.cern.ch/cp3-cms/pyplotit.git
pip install -e ./pyplotit
```

## Example: loading histograms from a plotIt configuration

If you do not have a plotIt configuration and the corresponding ROOT files
around, you can use the following commands to generate an example; they are
also used here for the rest of the example
% wget https://gitlab.cern.ch/cp3-cms/pyplotit/-/raw/master/tests/data/ex1_syst.yml

```{code-cell} ipython3
:tags: [hide-output]

!wget -q https://gitlab.cern.ch/cp3-cms/pyplotit/-/raw/master/tests/data/ex1_syst.yml
!wget -q https://raw.githubusercontent.com/cp3-llbb/plotIt/master/test/generate_files.C
!mkdir -p files
!root -l -b -q generate_files.C
```

We can load the configuration file ``ex1_syst.yml`` in pyplotit as follows:

```{code-cell} ipython3
import plotit
config, samples, plots, systematics, legend = plotit.loadFromYAML("ex1_syst.yml")
```

Most of the returned objects are either (lists of) simple objects that represent
a part of the configuration, e.g. a single plot.
The classes are implemented as
[data classes](https://docs.python.org/3/library/dataclasses.html).
The list returned in ``samples`` is based on the entries in the ``files``
block of the configuration file, but using the grouping specified by their
``group`` attributes and the list of groups, such that each entry corresponds
to a visible contribution in the plots.

Since the ``File`` and ``Group`` classes also contain functionality
for the efficient loading and summing of the histograms, the pure configuration
part is kept in a separate class (also a data class), under the ``cfg``
attribute.
For groups the list of grouped files can be found under ``files``.

```{code-cell} ipython3
[smp.cfg for smp in samples]
```

Typical plots contain an observed histogram and expectation stack.
Since the former may be the sum of multiple datasets, it is also handled as a
stack:

```{code-cell} ipython3
p = plots[0]
from plotit.plotit import Stack
expStack = Stack([smp.getHist(p) for smp in samples if smp.cfg.type == "MC"])
obsStack = Stack([smp.getHist(p) for smp in samples if smp.cfg.type == "DATA"])
```

The above works because both the ``File`` and ``Group`` class have a ``getHist``
method, which loads a single histogram from a file, or triggers the loading of
multiple histograms and adds them up, respectively.

``getHist`` returns a small object similar to a smart pointer: for a single file it holds the pointer to the (Py)ROOT histogram, for a group of stack it lazily constructs the sum histogram, or adds up the contents and squared weights arrays, depending on which method is called (more details will be added once the interfaces are more stable).
These smart pointer or histogram handle classes also implement the [uhi](https://uhi.readthedocs.io/) `PlottableHistogram` protocol, so they can directly be used with e.g. [mplhep](https://mplhep.readthedocs.io/):

```{code-cell} ipython3
from matplotlib import pyplot as plt
fig, ax = plt.subplots()
ax.set_xlim(*p.x_axis_range)
import mplhep
mplhep.histplot(obsStack, histtype="errorbar", color="k")
mplhep.histplot(expStack.entries, stack=True, histtype="fill", color=[e.style.fill_color for e in expStack.entries])
ax.set_xlabel(p.x_axis, loc="right")
ax.set_ylabel(p.y_axis, loc="top")
mplhep.cms.label(data=True, label="Internal", lumi=config.getLumi())
```
